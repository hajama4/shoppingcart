<?php

namespace App;

class TotalPrice
{
    private $lineParser;
    private $currentyConverter;

    public function __construct(LineParser $lineParser, CurrentyConverter $currentyConverter)
    {
        $this->lineParser = $lineParser;
        $this->currentyConverter = $currentyConverter;
    }

    public function totalPrice($data)
    {
        $totalPrice = 0;

        while(!feof($data)) {
            $parsedData = $this->lineParser->parseLine(fgets($data));
            if(is_numeric($parsedData[2]) && is_numeric($parsedData[3])) {
                $itemPrice = $this->currentyConverter->convertPrice($parsedData);
                $totalPrice += $parsedData[2] * $itemPrice;
            }
        }

        return round($totalPrice, 2);
    }
}

