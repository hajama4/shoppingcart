<?php

namespace App;

class CurrentyConverter
{
    public function convertPrice($data)
    {
        switch(trim($data[4])) {
            case 'USD':
                return $data[3] * 1.14;
            case 'GBP':
                return $data[3] * 0.88;
            default:
                return $data[3];
        }
    }
}

