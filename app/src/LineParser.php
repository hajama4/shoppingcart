<?php

namespace App;

class LineParser
{
    public function parseLine($line)
    {
        return explode(';', $line);
    }
}

