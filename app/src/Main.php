<?php

namespace App;

require 'vendor/autoload.php';

    $myfile = fopen("data/items.txt", "r") or die("Unable to open file!");

    $lineParser = new LineParser();
    $currentConverter = new CurrentyConverter();

    $totalPrice = new TotalPrice($lineParser, $currentConverter);

    printf("Cart price: %.2f EUR", $totalPrice->totalPrice($myfile));

    fclose($myfile);