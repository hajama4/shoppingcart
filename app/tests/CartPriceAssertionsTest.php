<?php

use \PHPUnit\Framework\TestCase;
use App\TotalPrice;
use App\LineParser;
use App\CurrentyConverter;

class CartPriceAssertionsTest extends TestCase
{
    public function testPrice()
    {
        $lineParser = new LineParser();
        $currentyConverter = new CurrentyConverter();
        $cartPrice = new TotalPrice($lineParser, $currentyConverter);
        $myfile = fopen("data/items.txt", "r") or die("Unable to open file!");

        $price = $cartPrice->totalPrice($myfile);

        $this->assertEquals(1513.54, $price);
        $this->assertNotEquals('1513.54', $price);
    }
}