## Getting Started

### Clone the repository
```bash
git clone https://hajama4@bitbucket.org/hajama4/shoppingcart.git
```

### Change directory
```bash
cd shoppingcart
```

### Command to launch docker
```bash
docker-compose up -d --build
```

### For windows to launch in command line to open a php container in which php commands can be executed
```bash
 docker exec -it shoppingCart-php-container bash
```
### Launch php script in docker container
```bash
php src/Main.php
```

### Launch phpunit tests in docker container
```bash
php vendor/bin/phpunit tests
```